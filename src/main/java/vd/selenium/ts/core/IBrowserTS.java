package vd.selenium.ts.core;

import org.openqa.selenium.WebDriver;

interface IBrowserTS
{
    void closeWebDriver();

    WebDriver getDriver();

    void openWebDriver();
}
