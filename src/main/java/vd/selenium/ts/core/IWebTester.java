package vd.selenium.ts.core;

import java.util.List;

import org.openqa.selenium.WebElement;

public interface IWebTester
{
    void click(String xpath, Object... args);

    WebElement find(String xpath, Object... args);

    WebElement findDisplayed(String xpath, Object... args);

    List<WebElement> findElements(String xpath, Object... args);

    String getText(String xpath, Object... args);

    void goPage(String url);

    boolean isPresence(String xpath, Object... args);

    void sendKeys(String xpath, String keysToSend, Object... args);

    public void setDefaultImplicitWait();
}
