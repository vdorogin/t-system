package vd.selenium.ts.core;

import static java.lang.String.format;
import static vd.selenium.ts.core.WaitTool.DEFAULT_WAIT_4_PAGE;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.inject.Inject;

/**
 * Implementation main methods:
 * <ul>
 *   <li>find</li>
 *   <li>findElements</li>
 *   <li>click</li>
 *   <li>getText</li>
 *   <li>sendKeys</li>
 * </ul>
 * @author vdorogin
 *
 */
class InnerTester implements IWebTester
{
    private IBrowserTS browser;

    @Inject
    public InnerTester(IBrowserTS browser)
    {
        this.browser = browser;
    }

    @Override
    public void click(String xpath, Object... args)
    {
        findDisplayed(xpath, args).click();
    }

    @Override
    public WebElement find(String xpath, Object... args)
    {
        return getDriver().findElement(By.xpath(format(xpath, args)));
    }

    @Override
    public WebElement findDisplayed(String xpath, Object... args)
    {
        for (WebElement webElement : findElements(xpath, args))
        {
            if (webElement.isDisplayed())
                return webElement;
        }
        throw new NoSuchElementException("Web element not found by xpath: " + format(xpath, args));
    }

    @Override
    public List<WebElement> findElements(String xpath, Object... args)
    {
        return getDriver().findElements(By.xpath(format(xpath, args)));
    }

    private IBrowserTS getBrowser()
    {
        return browser;
    }

    private WebDriver getDriver()
    {
        return getBrowser().getDriver();
    }

    @Override
    public String getText(String xpath, Object... args)
    {
        return findDisplayed(xpath, args).getText();
    }

    @Override
    public void goPage(String url)
    {
        getDriver().get(url);
    }

    @Override
    public boolean isPresence(String xpath, Object... args)
    {
        try
        {
            findDisplayed(xpath, args);
            return true;
        }
        catch (NoSuchElementException e)
        {
            return false;
        }
    }

    @Override
    public void sendKeys(String xpath, String keysToSend, Object... args)
    {
        find(xpath, args).sendKeys(keysToSend);
    }

    @Override
    public void setDefaultImplicitWait()
    {
        getDriver().manage().timeouts().implicitlyWait(DEFAULT_WAIT_4_PAGE, TimeUnit.SECONDS);
    }
}
