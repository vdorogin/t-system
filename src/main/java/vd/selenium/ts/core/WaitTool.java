package vd.selenium.ts.core;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

class WaitTool
{
    public static final long DEFAULT_WAIT_4_PAGE = 10L;
    public static final long REPETITION_PERIOD = 50L;

    /**
     * Checks if the List<WebElement> are in the DOM, regardless of being displayed or not.
     * 
     * @param driver - The driver object to use to perform this element search
     * @param by - selector to find the element
     * @return boolean
     */
    private static boolean areElementsPresent(WebDriver driver, By by)
    {
        try
        {
            driver.findElements(by);
            return true;
        }
        catch (NoSuchElementException e)
        {
            return false;
        }
    }

    /**
         * Checks if the text is present in the element. 
         * 
         * @param driver - The driver object to use to perform this element search
         * @param by - selector to find the element that should contain text
         * @param text - The Text element you are looking for
         * @return true or false
         */
    private static boolean isTextPresent(WebDriver driver, By by, String text)
    {
        try
        {
            return driver.findElement(by).getText().contains(text);
        }
        catch (NoSuchElementException e)
        {
            return false;
        }
    }

    public static boolean wait(WebDriver driver, long timeOutInSeconds, Function<WebDriver, Boolean> function)
            throws TimeoutException
    {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds, REPETITION_PERIOD);
        boolean result = wait.until(function);
        driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_4_PAGE, TimeUnit.SECONDS);
        return result;
    }

    public static WebElement waitElement(WebDriver driver, long timeOutInSeconds,
            Function<WebDriver, WebElement> function)
    {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds, REPETITION_PERIOD);
        WebElement webElement = wait.until(function);
        driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_4_PAGE, TimeUnit.SECONDS);
        return webElement;
    }

    /**
     * Wait for the element to be present in the DOM, and displayed on the page. 
     * And returns the first WebElement using the given method.
     * 
     * @param WebDriver    The driver object to be used 
     * @param By   selector to find the element
     * @param int  The time in seconds to wait until returning a failure
     *
     * @return WebElement  the first WebElement using the given method, or null (if the timeout is reached)
     */
    public static WebElement waitForElement(WebDriver driver, final By by, long timeOutInSeconds)
            throws TimeoutException
    {
        return waitElement(driver, timeOutInSeconds, ExpectedConditions.visibilityOfElementLocated(by));
    }

    /**
     * Wait for the element to be present in the DOM, regardless of being displayed or not.
     * And returns the first WebElement using the given method.
     *
     * @param WebDriver    The driver object to be used 
     * @param By   selector to find the element
     * @param int  The time in seconds to wait until returning a failure
     * 
     * @return WebElement  the first WebElement using the given method, or null (if the timeout is reached)
     */
    public static WebElement waitForElementPresent(WebDriver driver, final By by, long timeOutInSeconds)
    {
        return waitElement(driver, timeOutInSeconds, ExpectedConditions.presenceOfElementLocated(by));
    }

    public static void waitForJavaScriptCondition(WebDriver driver, final String javaScript)
    {
        wait(driver, DEFAULT_WAIT_4_PAGE, new ExpectedCondition<Boolean>()
        {
            @Override
            public Boolean apply(WebDriver driverObject)
            {
                return (Boolean)((JavascriptExecutor)driverObject).executeScript(javaScript);
            }
        });
    }

    /**
      * Wait for the List<WebElement> to be present in the DOM, regardless of being displayed or not.
      * Returns all elements within the current page DOM. 
      * 
      * @param WebDriver    The driver object to be used 
      * @param By   selector to find the element
      * @param int  The time in seconds to wait until returning a failure
      *
      * @return List<WebElement> all elements within the current page DOM, or null (if the timeout is reached)
      */
    public static List<WebElement> waitForListElementsPresent(WebDriver driver, final By by, long timeOutInSeconds)
    {
        List<WebElement> elements;
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS); //nullify implicitlyWait() 

        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until((new ExpectedCondition<Boolean>()
        {
            @Override
            public Boolean apply(WebDriver driverObject)
            {
                return areElementsPresent(driverObject, by);
            }
        }));

        elements = driver.findElements(by);
        driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_4_PAGE, TimeUnit.SECONDS); //reset implicitlyWait
        return elements; //return the element   
    }

    /**
    * Wait for the Text to be present in the given element, regardless of being displayed or not.
    *
    * @param WebDriver    The driver object to be used to wait and find the element
    * @param locator  selector of the given element, which should contain the text
    * @param String   The text we are looking
    * @param int  The time in seconds to wait until returning a failure
    * 
    * @return boolean 
    */
    public static boolean waitForTextPresent(WebDriver driver, final By by, final String text, long timeOutInSeconds)
    {
        return wait(driver, DEFAULT_WAIT_4_PAGE, new ExpectedCondition<Boolean>()
        {
            @Override
            public Boolean apply(WebDriver driverObject)
            {
                return isTextPresent(driverObject, by, text); //is the Text in the DOM
            }
        });
    }

    public static WebElement waitToBeClickable(WebDriver driver, final By by)
    {
        return waitElement(driver, DEFAULT_WAIT_4_PAGE, ExpectedConditions.elementToBeClickable(by));
    }
}
