package vd.selenium.ts.core;

import vd.selenium.ts.cases.gui.IGUIBoCard;
import vd.selenium.ts.cases.gui.IGUIDialog;
import vd.selenium.ts.cases.gui.IGUIForm;
import vd.selenium.ts.cases.gui.IGUILogon;
import vd.selenium.ts.cases.gui.IGUIMain;
import vd.selenium.ts.cases.gui.IGUINav;
import vd.selenium.ts.cases.gui.IGUIQuestionDialog;
import vd.selenium.ts.cases.gui.impl.GUIBoCard;
import vd.selenium.ts.cases.gui.impl.GUIDialog;
import vd.selenium.ts.cases.gui.impl.GUIForm;
import vd.selenium.ts.cases.gui.impl.GUILogon;
import vd.selenium.ts.cases.gui.impl.GUIMain;
import vd.selenium.ts.cases.gui.impl.GUINav;
import vd.selenium.ts.cases.gui.impl.GUIQuestionDialog;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class TesterModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        //Core service
        bind(IBrowserTS.class).to(BrowserTS.class);
        bind(IWebTester.class).to(WebTester.class);
        bind(IWebTester.class).annotatedWith(Inner.class).to(InnerTester.class);

        //Core Singleton
        bind(BrowserTS.class).in(Singleton.class);
        bind(WebTester.class).in(Singleton.class);
        bind(InnerTester.class).in(Singleton.class);
        bind(TestCase.class).in(Singleton.class);

        //GUI service
        bind(IGUIBoCard.class).to(GUIBoCard.class);
        bind(IGUIDialog.class).to(GUIDialog.class);
        bind(IGUIForm.class).to(GUIForm.class);
        bind(IGUILogon.class).to(GUILogon.class);
        bind(IGUIMain.class).to(GUIMain.class);
        bind(IGUINav.class).to(GUINav.class);
        bind(IGUIQuestionDialog.class).to(GUIQuestionDialog.class);

        //GUI Singleton
        bind(GUIBoCard.class).in(Singleton.class);
        bind(GUIDialog.class).in(Singleton.class);
        bind(GUIForm.class).in(Singleton.class);
        bind(GUILogon.class).in(Singleton.class);
        bind(GUIMain.class).in(Singleton.class);
        bind(GUINav.class).in(Singleton.class);
        bind(GUIQuestionDialog.class).in(Singleton.class);

        requestStaticInjection(TestCase.class);
    }
}
