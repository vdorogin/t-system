package vd.selenium.ts.core;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;

class BrowserTS implements IBrowserTS
{
    public enum WebBrowserType
    {
        FIREFOX, IEXPLORE, CHROME;
    }

    /**Время ожидания загрузки страницы, минуты*/
    private static final long PAGE_LOAD_TIME = 10;

    /**Веб драйвер*/
    private WebDriver driver = null;

    public BrowserTS()
    {
    }

    /**
     * Безопасно закрыть веб драйвер
     */
    @Override
    public void closeWebDriver()
    {
        if (driver != null)
        {
            driver.quit();
            driver = null;
        }
    }

    public void get(String url)
    {
        getDriver().get(url);
    }

    public String getCurrentUrl()
    {
        return getDriver().getCurrentUrl();
    }

    /**
    * Получить ссылку на веб драйвер
    */
    @Override
    public WebDriver getDriver()
    {
        return driver;
    }

    public String getTitle()
    {
        return getDriver().getTitle();
    }

    /**
     * Для взамодействия с Selenium хрому не достаточно просто ChromeDriver.
     * 
     * Требуется так называемый "chromedriver server"
     * который представляет из себя бинарный файл специфичный для OS.
     * 
     * Сейчас подключено два chromedriver server:
     * linux chromedriver x86
     * linux chromedriver x64
     *
     * Если потребуется поддержка Windows, то нужно скачать 
     * еще один chromeserver и указать путь запуска для него в CHROME_DRIVER_EXE_PROPERTY.
     * 
     * Сейчас этот путь всегда указывает на bash-скрипт, который в зависимости от битности
     * запускает соотвественно либо 32-битный драйвер, либо 64-битный для linux.
     * 
     * @return экземпляр ChromeDriver 
     */
    private WebDriver openChromeDriver()
    {
        //Установливает путь до места chromedriver server
        System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY,
                "src/main/resources/chromedriver/chromedriver");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");

        return new ChromeDriver(options);
    }

    /**
     * Открыть Firefox браузер
     * http://code.google.com/p/selenium/wiki/FirefoxDriver
     * @return возвращает экземпляр класса FirefoxDriver, реализующий интерфейс WebDriver
     * @exception WebDriverException если невозможно открыть браузер
     */
    private WebDriver openFirefox()
    {
        FirefoxProfile firefoxProfile = new FirefoxProfile();

        //Память на вкладки
        firefoxProfile.setPreference("browser.sessionhistory.max_total_viewer", "1");
        //Значение 3 не просто так, иначе не работает авторефреш
        firefoxProfile.setPreference("browser.sessionhistory.max_entries", 3);
        firefoxProfile.setPreference("browser.sessionhistory.max_total_viewers", 1);
        firefoxProfile.setPreference("browser.sessionstore.max_tabs_undo", 0);
        //Асинхронные запросы к серверу
        firefoxProfile.setPreference("network.http.pipelining", true);
        firefoxProfile.setPreference("network.http.pipelining.maxrequests", 8);
        //Задержка отрисовки
        firefoxProfile.setPreference("nglayout.initialpaint.delay", "0");
        //Сканирование внутренним сканером загнрузок
        firefoxProfile.setPreference("browser.download.manager.scanWhenDone", false);
        //Анимация переключения вкладок
        firefoxProfile.setPreference("browser.tabs.animate", false);
        //Автоподстановка
        firefoxProfile.setPreference("browser.search.suggest.enabled", false);
        //Анимация гифок
        firefoxProfile.setPreference("image.animation_mode", "none");
        //Резервные копии вкладок
        firefoxProfile.setPreference("browser.bookmarks.max_backups", 0);
        //Автодополнение
        firefoxProfile.setPreference("browser.formfill.enable", false);
        //Убрал дисковый кеш и кеш в памяти
        //firefoxProfile.setPreference("browser.cache.memory.enable", false);
        firefoxProfile.setPreference("browser.cache.disk.enable", false);
        //Сохранять файлы без подтверждения в tslogs
        firefoxProfile.setPreference("browser.download.folderList", 2);
        firefoxProfile.setPreference("browser.download.manager.showWhenStarting", false);

        //NSDPRD-2022
        firefoxProfile.setPreference("javascript.options.methodjit.content", false);

        firefoxProfile.setPreference("browser.download.dir", new File("").getAbsolutePath());
        firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk",
                "application/xml,application/pdf,application/zip,text/plain,application/vnd.ms-excel");
        return new FirefoxDriver(firefoxProfile);
    }

    /**
     * Открыть Internet Explorer браузер
     * @return возвращает экземпляр класса InternetExplorerDriver, реализующий интерфейс WebDriver
     * @exception WebDriverException если невозможно открыть браузер
     */
    private WebDriver openInternetExplorer()
    {
        return new InternetExplorerDriver();
    }

    /**
     * Получить веб драйвер (открыть браузер)
     * <p>
     * Перед открытием браузера убеждаемся в том, что он точно закрыт (если был открыт, то закрываем его)
     * </p>
     */
    @Override
    public void openWebDriver()
    {
        try
        {
            closeWebDriver();
            String browserType = System.getProperty("browser") == null ? WebBrowserType.FIREFOX.toString() : System
                    .getProperty("browser");

            switch (WebBrowserType.valueOf(browserType.toUpperCase()))
            {
            case FIREFOX:
                driver = openFirefox();
                break;
            case IEXPLORE:
                driver = openInternetExplorer();
                break;
            case CHROME:
                driver = openChromeDriver();
                break;
            default:
                driver = openFirefox();
            }
        }
        catch (WebDriverException e)
        {
            e.printStackTrace();
        }
        //Устанавливаем максимальное время ожидания загрузки страницы
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIME, TimeUnit.MINUTES);
        driver.manage().timeouts().implicitlyWait(WaitTool.DEFAULT_WAIT_4_PAGE, TimeUnit.SECONDS);
    }

}
