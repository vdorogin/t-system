package vd.selenium.ts.core;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import vd.selenium.ts.core.GuiceJUnitRunner.GuiceModules;

import com.google.inject.Inject;

@RunWith(GuiceJUnitRunner.class)
@GuiceModules({ TesterModule.class })
public class TestCase
{
    private static final String VERIFY_TS = "verifyTS";

    @Inject
    private static IBrowserTS browser;

    @AfterClass
    public static void afterClass()
    {
        browser.closeWebDriver();
    }

    @BeforeClass
    public static void beforeClass()
    {
        if (!Boolean.TRUE.toString().equals(System.getProperty(VERIFY_TS)))
        {
            browser.openWebDriver();
            System.setProperty(VERIFY_TS, "true");
        }
    }

    @After
    public void after()
    {
    }

    @Before
    public void before()
    {
    }
}
