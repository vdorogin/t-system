package vd.selenium.ts.core;

import static java.lang.String.format;
import static vd.selenium.ts.core.WaitTool.DEFAULT_WAIT_4_PAGE;
import static vd.selenium.ts.core.WaitTool.waitForElement;
import static vd.selenium.ts.core.WaitTool.waitForElementPresent;
import static vd.selenium.ts.core.WaitTool.waitForListElementsPresent;
import static vd.selenium.ts.core.WaitTool.waitToBeClickable;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.inject.Inject;

class WebTester implements IWebTester
{
    public static final long REPETITION_PERIOD = 50L;

    private IBrowserTS browser;

    @Inject
    public WebTester(IBrowserTS browser)
    {
        this.browser = browser;
    }

    private By byXpath(String xpath, Object... args)
    {
        return By.xpath(format(xpath, args));
    }

    @Override
    public void click(String xpath, Object... args)
    {
        waitToBeClickable(getDriver(), byXpath(xpath, args)).click();
    }

    @Override
    public WebElement find(String xpath, Object... args)
    {
        return waitForElementPresent(getDriver(), byXpath(xpath, args), DEFAULT_WAIT_4_PAGE);
    }

    @Override
    public WebElement findDisplayed(String xpath, Object... args)
    {
        return waitForElement(getDriver(), byXpath(xpath, args), DEFAULT_WAIT_4_PAGE);
    }

    @Override
    public List<WebElement> findElements(String xpath, Object... args)
    {
        return waitForListElementsPresent(getDriver(), byXpath(xpath, args), DEFAULT_WAIT_4_PAGE);
    }

    private IBrowserTS getBrowser()
    {
        return browser;
    }

    private WebDriver getDriver()
    {
        return getBrowser().getDriver();
    }

    @Override
    public String getText(String xpath, Object... args)
    {
        return waitForElementPresent(getDriver(), byXpath(xpath, args), DEFAULT_WAIT_4_PAGE).getText();
    }

    @Override
    public void goPage(String url)
    {
        getBrowser().getDriver().get(url);
    }

    @Override
    public boolean isPresence(String xpath, Object... args)
    {
        try
        {
            waitForElement(getDriver(), byXpath(xpath, args), DEFAULT_WAIT_4_PAGE);
            return true;
        }
        catch (TimeoutException e)
        {
            return false;
        }
    }

    @Override
    public void sendKeys(String xpath, String keysToSend, Object... args)
    {
        waitForElementPresent(getDriver(), byXpath(xpath, args), DEFAULT_WAIT_4_PAGE).sendKeys(keysToSend);
    }

    @Override
    public void setDefaultImplicitWait()
    {
        getDriver().manage().timeouts().implicitlyWait(DEFAULT_WAIT_4_PAGE, TimeUnit.SECONDS);
    }
}
