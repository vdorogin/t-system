package vd.selenium.ts.cases.matcher;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import vd.selenium.ts.cases.gui.IGUIForm;

public class FormMatcher extends TypeSafeMatcher<IGUIForm>
{
    @Factory
    public static Matcher<IGUIForm> isOpen()
    {
        return new FormMatcher();
    }

    @Override
    public void describeMismatchSafely(IGUIForm form, Description mismatchDescription)
    {
        mismatchDescription.appendText("Форма не открылась");
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("Открылась форма");
    }

    @Override
    protected boolean matchesSafely(IGUIForm form)
    {
        return form.isOpen();
    }

}
