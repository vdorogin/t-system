package vd.selenium.ts.cases.gui;

import static vd.selenium.ts.cases.gui.impl.GUIMain.STAND_URL;
import vd.selenium.ts.cases.gui.impl.GUINav;

public interface IGUINav
{
    public static final String OPERATOR = STAND_URL + "operator/";
    public static final String ADMIN = STAND_URL + "admin/";
    public static final String TAB = "//div[contains(@id, 'gwt-debug-') and text()='%s']";
    public static final String BO_CARD = OPERATOR + "#uuid:%s";

    IGUIBoCard admin();

    IGUIBoCard bo(String uuid);

    GUINav link(String url, Object... args);

    IGUIBoCard operator();

    IGUIBoCard tab(String tabName);
}
