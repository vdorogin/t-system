package vd.selenium.ts.cases.gui.impl;

import static java.lang.String.format;

import org.openqa.selenium.TimeoutException;

import vd.selenium.ts.cases.gui.IGUIBoCard;
import vd.selenium.ts.cases.gui.IGUIDialog;
import vd.selenium.ts.cases.gui.IGUIForm;
import vd.selenium.ts.cases.gui.IGUIQuestionDialog;

public class GUIBoCard extends GUINav implements IGUIBoCard
{
    @Override
    public IGUIForm addSc()
    {
        return clickButton(BTN_ADD_SC, "Добавить запрос");
    }

    private IGUIForm clickButton(String btnXpath, String btnTitle)
    {
        try
        {
            getTester().click(btnXpath);
        }
        catch (TimeoutException e)
        {
            getTester().setDefaultImplicitWait();
            throw new RuntimeException(format("Отсутствует кнопка '%s' на карточке БО.", btnTitle), e);
        }
        return form;
    }

    @Override
    public IGUIQuestionDialog delete()
    {
        getTester().click(BTN_DELETE);
        return questionDialog;
    }

    @Override
    public IGUIForm edit()
    {
        return clickButton(BTN_EDIT, "Редкатировать");
    }

    @Override
    public IGUIDialog move()
    {
        getTester().click(BTN_MOVE);
        return dialog;
    }

}
