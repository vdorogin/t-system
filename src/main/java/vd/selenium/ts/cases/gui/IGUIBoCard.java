package vd.selenium.ts.cases.gui;

import static java.lang.String.format;

public interface IGUIBoCard extends IGUINav
{
    static final String BTN = "//div[contains(@id, 'gwt-debug-ToolPanel.')]//div[contains(@id, 'gwt-debug-ActionTool.%s.')]";

    static final String BTN_ADD_SC = format(BTN, "addSC");
    static final String BTN_EDIT = format(BTN, "edit");
    static final String BTN_MOVE = format(BTN, "move");
    static final String BTN_DELETE = format(BTN, "del");

    IGUIForm addSc();

    IGUIQuestionDialog delete();

    IGUIForm edit();

    IGUIDialog move();
}
