package vd.selenium.ts.cases.gui;

public interface IGUILogon
{
    static final String ID_INPUT_FIELD_USERNAME = "//*[@id='username']";
    static final String ID_INPUT_FIELD_PASSWORD = "//*[@id='password']";

    static final String ID_BUTTON_LOGIN = "//*[@type='submit']";

    void asSuper();

    void login(String login, String password);
}
