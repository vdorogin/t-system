package vd.selenium.ts.cases.gui.impl;

import vd.selenium.ts.cases.gui.IGUIForm;

public class GUIForm extends GUIMain implements IGUIForm
{
    @Override
    public boolean isOpen()
    {
        return getTester().isPresence(FORM);
    }
}
