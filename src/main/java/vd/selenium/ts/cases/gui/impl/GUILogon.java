package vd.selenium.ts.cases.gui.impl;

import vd.selenium.ts.cases.gui.IGUILogon;

public class GUILogon extends GUIMain implements IGUILogon
{
    @Override
    public void asSuper()
    {
        login("naumen", "n@usd40");
    }

    @Override
    public void login(String login, String password)
    {
        goPage(STAND_URL);
        getInnerTester().sendKeys(ID_INPUT_FIELD_USERNAME, login);
        getInnerTester().sendKeys(ID_INPUT_FIELD_PASSWORD, password);
        getInnerTester().find(ID_BUTTON_LOGIN).submit();
    }
}
