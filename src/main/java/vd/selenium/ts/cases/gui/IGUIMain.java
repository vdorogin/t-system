package vd.selenium.ts.cases.gui;

public interface IGUIMain
{
    void goPage(String url, Object... args);

    boolean isPresence(String xpath, Object... args);
}
