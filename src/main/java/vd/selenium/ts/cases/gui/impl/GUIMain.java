package vd.selenium.ts.cases.gui.impl;

import static java.lang.String.format;
import vd.selenium.ts.cases.gui.IGUIBoCard;
import vd.selenium.ts.cases.gui.IGUIDialog;
import vd.selenium.ts.cases.gui.IGUIForm;
import vd.selenium.ts.cases.gui.IGUILogon;
import vd.selenium.ts.cases.gui.IGUIMain;
import vd.selenium.ts.cases.gui.IGUIQuestionDialog;
import vd.selenium.ts.core.IWebTester;
import vd.selenium.ts.core.Inner;

import com.google.inject.Inject;

public class GUIMain implements IGUIMain
{
    public static final String STAND_URL = "http://localhost:12345/sd/";

    @Inject
    public IGUIForm form;
    @Inject
    public IGUIDialog dialog;
    @Inject
    public IGUIQuestionDialog questionDialog;
    @Inject
    public IGUIBoCard card;
    @Inject
    public GUINav navigation;
    @Inject
    public IGUILogon logon;
    @Inject
    public IGUIMain mainService;

    @Inject
    private IWebTester tester;

    @Inject
    @Inner
    private IWebTester innerTester;

    public GUIMain()
    {

    }

    protected IWebTester getInnerTester()
    {
        return innerTester;
    }

    protected IWebTester getTester()
    {
        return tester;
    }

    @Override
    public void goPage(String url, Object... args)
    {
        getTester().goPage(format(url, args));
    }

    @Override
    public boolean isPresence(String xpath, Object... args)
    {
        return getTester().isPresence(xpath, args);
    }
}
