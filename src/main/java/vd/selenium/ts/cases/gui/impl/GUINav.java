package vd.selenium.ts.cases.gui.impl;

import vd.selenium.ts.cases.gui.IGUIBoCard;
import vd.selenium.ts.cases.gui.IGUINav;

public class GUINav extends GUIMain implements IGUINav
{
    @Override
    public IGUIBoCard admin()
    {
        return go(ADMIN);
    }

    @Override
    public IGUIBoCard bo(String uuid)
    {
        return go(BO_CARD, uuid);
    }

    private IGUIBoCard go(String url, Object... args)
    {
        goPage(url, args);
        return card;
    }

    @Override
    public GUINav link(String url, Object... args)
    {
        goPage(url, args);
        return navigation;
    }

    @Override
    public IGUIBoCard operator()
    {
        return go(OPERATOR);
    }

    @Override
    public IGUIBoCard tab(String tabName)
    {
        getTester().click(TAB, tabName);
        return card;
    }

}
