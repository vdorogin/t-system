package vd.selenium.ts.cases.gui;

public interface IGUIForm
{
    static final String FORM = "//div[@id='gwt-debug-Form']";

    boolean isOpen();
}
