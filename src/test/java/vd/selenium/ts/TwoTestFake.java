package vd.selenium.ts;

import org.junit.Assert;
import org.junit.Test;

public class TwoTestFake
{
    @Test
    public void testFalse()
    {
        Assert.assertFalse(false);
    }

    @Test
    public void testSystemProperty()
    {
        Assert.assertNotNull(System.getProperty("notNullProperty"));
    }
}
