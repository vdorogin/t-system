package vd.selenium.ts;

import org.junit.Assert;
import org.junit.Test;

public class OneTestFake
{
    @Test
    public void testSystemProperty()
    {
        Assert.assertNull(System.getProperty("nullProperty"));
    }

    @Test
    public void testTrue()
    {
        Assert.assertTrue(true);
    }
}
