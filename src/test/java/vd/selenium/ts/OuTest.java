package vd.selenium.ts;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static vd.selenium.ts.cases.gui.impl.GUIMain.STAND_URL;
import static vd.selenium.ts.cases.matcher.FormMatcher.isOpen;

import org.junit.Test;

import vd.selenium.ts.cases.gui.IGUIBoCard;
import vd.selenium.ts.cases.gui.IGUIForm;
import vd.selenium.ts.cases.gui.IGUILogon;
import vd.selenium.ts.cases.gui.IGUIMain;
import vd.selenium.ts.cases.gui.impl.GUINav;
import vd.selenium.ts.core.TestCase;

import com.google.inject.Inject;

public class OuTest extends TestCase
{
    @Inject
    IGUIForm form;
    @Inject
    IGUIBoCard card;
    @Inject
    GUINav navigation;
    @Inject
    IGUILogon logon;
    @Inject
    IGUIMain mainService;

    @Test
    public void testEdit()
    {
        //Подготовка
        String uuid = "ou$301";
        //Выполнение действий
        logon.asSuper();
        card.bo(uuid).edit();
        //Проверка
        assertThat(form, isOpen());
    }

    @Test
    public void testSecretCheckStatus()
    {
        //Выполнение действий
        navigation.link(STAND_URL + "services/rest/40x-secret-check-status");
        //Проверка
        assertThat(mainService.isPresence("//*[text()='Операция выполнена успешно']"), is(true));
    }
}
