package vd.selenium.ts;

import org.junit.Test;

import vd.selenium.ts.cases.gui.IGUILogon;
import vd.selenium.ts.cases.gui.IGUINav;
import vd.selenium.ts.core.TestCase;

import com.google.inject.Inject;

public class InitTest extends TestCase
{
    @Inject
    IGUILogon logon;
    @Inject
    IGUINav nav;

    @Test
    public void testLogin()
    {
        logon.login("naumen", "n@usd40");
    }

    @Test
    public void testNavigation()
    {
        logon.login("naumen", "n@usd40");

        String uuid = "ou$301";

        nav.operator().tab("Оргструктура").tab("Запросы");
        nav.bo(uuid).tab("История").tab("Отдел").operator();
        nav.tab("Соглашения и услуги").tab("Атрибуты компании").tab("Оргструктура");

        nav.admin().operator().admin().operator();
        nav.bo(uuid).tab("История").tab("Отдел").operator().bo(uuid).tab("История").tab("Отдел");
    }
}
